using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ClickedButton : MonoBehaviour {

    public GameObject loadingImage;
    public GameObject titleMenu;
    public GameObject selectMenu;

    public Sprite audioOn, audioOff;
    public Button audioBtn;

    public AudioSource audio;

    void Start()
    {
        audio.GetComponent<AudioSource>();
    }

    public void OnClick(int set)
    {
        if (set == 0)
        {
            titleMenu.SetActive(false);
            selectMenu.SetActive(true);
        } else if (set == 1)
        {
            titleMenu.SetActive(true);
            selectMenu.SetActive(false);
        }
        
    }

    public void LoadScene(int level)
    {
        loadingImage.SetActive(true);
        Application.LoadLevel(level);
    }

    public void Mute()
    {
        if (audio.mute)
        {
            audio.mute = false;
            audioBtn.image.sprite = audioOn;
        }
        else
        {
            audio.mute = true;
            audioBtn.image.sprite = audioOff;
        }
    }
}
